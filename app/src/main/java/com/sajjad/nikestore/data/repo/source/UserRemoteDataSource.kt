package com.sajjad.nikestore.data.repo.source

import com.google.gson.JsonObject
import com.sajjad.nikestore.services.http.ApiService
import com.sajjad.nikestore.data.MessageResponse
import com.sajjad.nikestore.data.TokenResponse
import io.reactivex.Single

const val CLIENT_ID = 2
const val CLIENT_SECRET = "kyj1c9sVcksqGU4scMX7nLDalkjp2WoqQEf8PKAC"

class UserRemoteDataSource(val apiService: ApiService) :UserDataSource {
    override fun signUp(email: String, password: String): Single<MessageResponse> {
       return apiService.signUp(JsonObject().apply {
            addProperty("email",email)
            addProperty("password",password)
        })
    }

    override fun login(email: String, password: String): Single<TokenResponse> {
       return apiService.login(JsonObject().apply {
            addProperty("grant_type","password")
            addProperty("client_id", CLIENT_ID)
            addProperty("client_secret", CLIENT_SECRET)
            addProperty("username",email)
            addProperty("password",password)
        })
    }

    override fun saveToken(token: String, refreshToken: String) {
        TODO("Not yet implemented")
    }

    override fun loadToken() {
        TODO("Not yet implemented")
    }

    override fun saveUserName(username: String) {
        TODO("Not yet implemented")
    }

    override fun getUserName(): String {
        TODO("Not yet implemented")
    }

    override fun signOut() {
        TODO("Not yet implemented")
    }
}