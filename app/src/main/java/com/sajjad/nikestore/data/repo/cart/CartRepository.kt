package com.sajjad.nikestore.data.repo.cart

import com.sajjad.nikestore.data.AddToCartResponse
import com.sajjad.nikestore.data.CartItemCount
import com.sajjad.nikestore.data.CartResponse
import com.sajjad.nikestore.data.MessageResponse
import io.reactivex.Single

interface CartRepository {

    fun add(productId: Int): Single<AddToCartResponse>
    fun remove(cartItemId: Int): Single<MessageResponse>
    fun get(): Single<CartResponse>
    fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse>
    fun getCartCount(): Single<CartItemCount>

}