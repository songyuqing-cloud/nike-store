package com.sajjad.nikestore.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.data.repo.source.ProductLocalDataSource

@Database(entities = [Product::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getDao(): ProductLocalDataSource
}