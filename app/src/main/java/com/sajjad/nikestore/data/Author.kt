package com.sajjad.nikestore.data

data class Author(
    val email: String
)