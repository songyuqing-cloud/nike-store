package com.sajjad.nikestore.data.repo.source

import com.sajjad.nikestore.data.Banner
import com.sajjad.nikestore.services.http.ApiService
import io.reactivex.Single

class BannerRemoteDataSource(val apiService: ApiService) : BannerDataSource {
    override fun getBanners(): Single<List<Banner>> = apiService.getBanners()
}