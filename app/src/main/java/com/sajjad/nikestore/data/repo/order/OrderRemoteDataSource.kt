package com.sajjad.nikestore.data.repo.order

import com.google.gson.JsonObject
import com.sajjad.nikestore.data.Checkout
import com.sajjad.nikestore.data.SubmitOrderResponse
import com.sajjad.nikestore.services.http.ApiService
import io.reactivex.Single

class OrderRemoteDataSource(val apiService: ApiService) : OrderDataSource {
    override fun submit(
        firstName: String,
        lastName: String,
        postalCode: String,
        phoneNumber: String,
        address: String,
        paymentMethod: String
    ): Single<SubmitOrderResponse> = apiService.submitOrder(JsonObject().apply {
        addProperty("first_name", firstName)
        addProperty("last_name", lastName)
        addProperty("postal_code", postalCode)
        addProperty("mobile", phoneNumber)
        addProperty("address", address)
        addProperty("payment_method", paymentMethod)
    })

    override fun checkOut(orderId: Int): Single<Checkout> = apiService.checkOut(orderId)

}