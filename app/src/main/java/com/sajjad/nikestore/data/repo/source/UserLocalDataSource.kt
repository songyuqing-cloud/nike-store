package com.sajjad.nikestore.data.repo.source

import android.content.SharedPreferences
import com.sajjad.nikestore.common.ACCESS_TOKEN_KEY
import com.sajjad.nikestore.common.REFRESH_TOKEN_KEY
import com.sajjad.nikestore.data.TokenContainer
import com.sajjad.nikestore.data.MessageResponse
import com.sajjad.nikestore.data.TokenResponse
import io.reactivex.Single

class UserLocalDataSource(val sharedPreferences: SharedPreferences) : UserDataSource {
    override fun signUp(email: String, password: String): Single<MessageResponse> {
        TODO("Not yet implemented")
    }

    override fun login(email: String, password: String): Single<TokenResponse> {
        TODO("Not yet implemented")
    }

    override fun saveToken(token: String, refreshToken: String) {
        sharedPreferences.edit().apply {
            putString(ACCESS_TOKEN_KEY, token)
            putString(REFRESH_TOKEN_KEY, refreshToken)
        }.apply()
    }

    override fun loadToken() {
        TokenContainer.update(
            sharedPreferences.getString(ACCESS_TOKEN_KEY, null),
            sharedPreferences.getString(REFRESH_TOKEN_KEY, null)
        )

    }

    override fun saveUserName(username: String) {
        sharedPreferences.edit().apply {
            putString("username", username)
        }.apply()
    }


    override fun getUserName(): String = sharedPreferences.getString("username", "") ?: ""
    override fun signOut() {
        sharedPreferences.edit().apply {
            clear()
        }.apply()
    }
}