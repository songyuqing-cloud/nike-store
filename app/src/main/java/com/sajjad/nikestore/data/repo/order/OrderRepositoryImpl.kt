package com.sajjad.nikestore.data.repo.order

import com.sajjad.nikestore.data.Checkout
import com.sajjad.nikestore.data.SubmitOrderResponse
import io.reactivex.Single

class OrderRepositoryImpl(val orderDataSource: OrderDataSource):OrderRepository {
    override fun submit(
        firstName: String,
        lastName: String,
        postalCode: String,
        phoneNumber: String,
        address: String,
        paymentMethod: String
    ): Single<SubmitOrderResponse> {
        return orderDataSource.submit(firstName,lastName,postalCode,phoneNumber, address,paymentMethod)
    }

    override fun checkOut(orderId: Int): Single<Checkout> {
        return orderDataSource.checkOut(orderId)
    }
}