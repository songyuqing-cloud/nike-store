package com.sajjad.nikestore.data.repo.source

import com.google.gson.JsonObject
import com.sajjad.nikestore.data.AddToCartResponse
import com.sajjad.nikestore.services.http.ApiService
import com.sajjad.nikestore.data.CartItemCount
import com.sajjad.nikestore.data.CartResponse
import com.sajjad.nikestore.data.MessageResponse
import io.reactivex.Single

class CartRemoteDataSource(val apiService: ApiService) : CartDataSource {

    override fun add(productId: Int): Single<AddToCartResponse> = apiService.addToCart(
        JsonObject().apply {
            addProperty("product_id",productId)
        }
    )

    override fun remove(cartItemId: Int): Single<MessageResponse> = apiService.removeFromCart(JsonObject().apply {
        addProperty("cart_item_id",cartItemId)
    })

    override fun get(): Single<CartResponse> = apiService.getCartItems()

    override fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse> = apiService.changeCartCount(JsonObject().apply {
        addProperty("cart_item_id",cartItemId)
        addProperty("count",count)
    })

    override fun getCartCount(): Single<CartItemCount> = apiService.getCartCount()
}