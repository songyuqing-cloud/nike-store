package com.sajjad.nikestore.data.repo.banner

import com.sajjad.nikestore.data.Banner
import io.reactivex.Single

interface BannerRepository {

    fun getBanners():Single<List<Banner>>
}