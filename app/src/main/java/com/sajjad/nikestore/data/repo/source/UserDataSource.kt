package com.sajjad.nikestore.data.repo.source

import com.sajjad.nikestore.data.MessageResponse
import com.sajjad.nikestore.data.TokenResponse
import io.reactivex.Single

interface UserDataSource {

    fun signUp(email: String, password: String): Single<MessageResponse>

    fun login(email: String, password: String): Single<TokenResponse>

    fun saveToken(token: String, refreshToken: String)

    fun loadToken()

    fun saveUserName(username: String)

    fun getUserName(): String

    fun signOut()
}