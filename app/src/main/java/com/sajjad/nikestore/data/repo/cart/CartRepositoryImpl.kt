package com.sajjad.nikestore.data.repo.cart

import com.sajjad.nikestore.data.AddToCartResponse
import com.sajjad.nikestore.data.repo.source.CartDataSource
import com.sajjad.nikestore.data.CartItemCount
import com.sajjad.nikestore.data.CartResponse
import com.sajjad.nikestore.data.MessageResponse
import io.reactivex.Single

class CartRepositoryImpl(val cartRemoteDataSource: CartDataSource): CartRepository {
    override fun add(productId: Int): Single<AddToCartResponse>  = cartRemoteDataSource.add(productId)

    override fun remove(cartItemId: Int): Single<MessageResponse> = cartRemoteDataSource.remove(cartItemId)

    override fun get(): Single<CartResponse> = cartRemoteDataSource.get()

    override fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse> = cartRemoteDataSource.changeCount(cartItemId,count)

    override fun getCartCount(): Single<CartItemCount> = cartRemoteDataSource.getCartCount()
}