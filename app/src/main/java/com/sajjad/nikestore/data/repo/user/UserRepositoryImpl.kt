package com.sajjad.nikestore.data.repo.user

import com.sajjad.nikestore.data.TokenContainer
import com.sajjad.nikestore.data.repo.source.UserDataSource

import com.sajjad.nikestore.data.TokenResponse
import io.reactivex.Completable

class UserRepositoryImpl(
    val remoteDataSource: UserDataSource,
    val localDataSource: UserDataSource
) : UserRepository {
    override fun signUp(email: String, password: String): Completable {
        return remoteDataSource.signUp(email, password).flatMap {
            remoteDataSource.login(email, password)
        }.doOnSuccess {
            onLoginSuccess(email, it)
        }.ignoreElement()
    }

    override fun login(email: String, password: String): Completable {
        return remoteDataSource.login(email, password).doOnSuccess {
            onLoginSuccess(email,it)
        }.ignoreElement()
    }


    override fun loadToken() {
        localDataSource.loadToken()
    }

    override fun getUserName(): String = localDataSource.getUserName()

    override fun signOut() {
        localDataSource.signOut()
        TokenContainer.update(null,null)
    }


    fun onLoginSuccess(username: String, tokenResponse: TokenResponse) {
        TokenContainer.update(tokenResponse.access_token, tokenResponse.refresh_token)
        localDataSource.saveToken(tokenResponse.access_token, tokenResponse.refresh_token)
        localDataSource.saveUserName(username)
    }
}