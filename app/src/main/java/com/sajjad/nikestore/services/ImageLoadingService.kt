package com.sajjad.nikestore.services

import com.sajjad.nikestore.view.NikeImageView

interface ImageLoadingService {
    fun load(imageView: NikeImageView, imageUrl:String)
}