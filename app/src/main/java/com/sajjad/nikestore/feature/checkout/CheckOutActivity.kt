package com.sajjad.nikestore.feature.checkout

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.common.NikeActivity
import com.sajjad.nikestore.common.formatPrice
import com.sajjad.nikestore.feature.home.HomeFragment
import com.sajjad.nikestore.feature.main.MainActivity
import kotlinx.android.synthetic.main.activity_check_out.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CheckOutActivity : NikeActivity() {
    val viewModel :CheckoutViewModel by viewModel {
        val uri: Uri? = intent.data
        if (uri!=null)
            parametersOf(uri.getQueryParameter("order_id")!!.toInt())
        else
            parametersOf(intent.extras!!.getInt(EXTRA_KEY_DATA))

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_out)

        checkout_toolbar.onBackClickListener = View.OnClickListener {
            finish()
        }

        viewModel.checkoutLiveData.observe(this){
            purchase_status_tv.text = if (it.purchase_success) "پرداخت با موفقیت انجام شد" else "پرداخت ناموفق"
            order_status_tv.text = it.payment_status
            order_price_tv.text = formatPrice(it.payable_price)
        }

        return_home_btn.setOnClickListener {
            finish()

        }
    }
}