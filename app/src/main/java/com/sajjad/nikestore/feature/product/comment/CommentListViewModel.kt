package com.sajjad.nikestore.feature.product.comment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.common.asyncNetworkRequest
import com.sajjad.nikestore.data.Comment
import com.sajjad.nikestore.data.repo.comment.CommentRepository


class CommentListViewModel(commentRepository: CommentRepository, productId: Int) : NikeViewModel() {

    private val _commentsLiveData = MutableLiveData<List<Comment>>()
    val commentsLiveData: LiveData<List<Comment>>
        get() = _commentsLiveData

    init {
        progressLiveData.value = true
        commentRepository.getAll(productId)
            .asyncNetworkRequest()
            .doFinally{
                progressLiveData.value = false
            }

            .subscribe(object : NikeSingleObserver<List<Comment>>(compositeDisposable) {
                override fun onSuccess(t: List<Comment>) {
                    _commentsLiveData.value = t
                }

            })
    }
}