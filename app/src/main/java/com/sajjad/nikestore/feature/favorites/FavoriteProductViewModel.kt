package com.sajjad.nikestore.feature.favorites

import androidx.lifecycle.MutableLiveData
import com.sajjad.nikestore.common.NikeCompletableObserver
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.data.repo.product.ProductRepository
import io.reactivex.schedulers.Schedulers

class FavoriteProductViewModel(private val productRepository: ProductRepository):NikeViewModel() {

    val favProductLiveData = MutableLiveData<List<Product>>()
    init {
        productRepository.getFavoriteProducts()
            .subscribeOn(Schedulers.io())
            .subscribe(object :NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    favProductLiveData.postValue(t)
                }
            })
    }

    fun add(product: Product){
        productRepository.addToFavorite(product)
            .subscribeOn(Schedulers.io())
            .subscribe(object :NikeCompletableObserver(compositeDisposable){
                override fun onComplete() {

                }
            })
    }

    fun remove(product: Product){
        productRepository.deleteFromFavorites(product)
            .subscribeOn(Schedulers.io())
            .subscribe(object :NikeCompletableObserver(compositeDisposable){
                override fun onComplete() {

                }
            })
    }
}