package com.sajjad.nikestore.feature.product

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.common.asyncNetworkRequest
import com.sajjad.nikestore.data.Comment
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.data.repo.cart.CartRepository
import com.sajjad.nikestore.data.repo.comment.CommentRepository
import io.reactivex.Completable

class ProductDetailViewModel(bundle: Bundle, commentRepository: CommentRepository, val cartRepository: CartRepository) :
    NikeViewModel() {

    private val _productLiveData = MutableLiveData<Product>()
    val productLiveData: LiveData<Product>
        get() = _productLiveData

    private val _commentsLiveData = MutableLiveData<List<Comment>>()
    val commentsLiveData: LiveData<List<Comment>>
        get() = _commentsLiveData

    init {
        progressLiveData.value = true

        _productLiveData.value = bundle.getParcelable(EXTRA_KEY_DATA)

        commentRepository.getAll(_productLiveData.value!!.id)
            .asyncNetworkRequest()
            .doFinally {
                progressLiveData.value = false
            }
            .subscribe(object : NikeSingleObserver<List<Comment>>(compositeDisposable) {
                override fun onSuccess(t: List<Comment>) {
                    _commentsLiveData.value = t
                }

            })
    }
    fun onAddCartBtn():Completable = cartRepository.add(productLiveData.value!!.id).ignoreElement()

}

