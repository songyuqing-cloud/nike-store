package com.sajjad.nikestore.feature.shipping

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.openUrlInCustomTab
import com.sajjad.nikestore.data.SubmitOrderResponse
import com.sajjad.nikestore.feature.cart.CartItemAdapter
import com.sajjad.nikestore.feature.checkout.CheckOutActivity
import com.sajjad.nikestore.data.PurchaseDetail
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_shipping.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.lang.IllegalStateException

class ShippingActivity : AppCompatActivity() {

    val viewModel: ShippingViewModel by viewModel()
    val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipping)

        val purchaseDetail = intent.getParcelableExtra<PurchaseDetail>(EXTRA_KEY_DATA)
            ?: throw IllegalStateException("purchase detail cannot be null")

        val viewHolder = CartItemAdapter.PurchaseDetailViewHolder(purchase_detail)

        viewHolder.bindPurchaseDetail(
            purchaseDetail.total_price,
            purchaseDetail.shipping_cost,
            purchaseDetail.payable_price
        )

        val onClick = View.OnClickListener {
            viewModel.submitOrder(
                first_name_edt.text.toString(),
                last_name_edt.text.toString(),
                postal_code_edt.text.toString(),
                phone_number_edt.text.toString(),
                address_edt.text.toString(),
                if (it.id == R.id.online_payment_btn) PAYMENT_METHOD_ONLINE else PAYMENT_METHOD_COD
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : NikeSingleObserver<SubmitOrderResponse>(compositeDisposable) {
                    override fun onSuccess(t: SubmitOrderResponse) {
                        if (t.bank_gateway_url.isNotEmpty()) {
                            openUrlInCustomTab(this@ShippingActivity, t.bank_gateway_url)
                        } else {
                            startActivity(
                                Intent(
                                    this@ShippingActivity,
                                    CheckOutActivity::class.java
                                ).apply {
                                    putExtra(EXTRA_KEY_DATA, t.order_id)
                                })
                        }

                        finish()
                    }
                })
        }

        online_payment_btn.setOnClickListener(onClick)
        cod_btn.setOnClickListener(onClick)

        nike_toolbar.onBackClickListener = View.OnClickListener {
            finish()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}