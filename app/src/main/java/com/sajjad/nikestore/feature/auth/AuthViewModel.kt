package com.sajjad.nikestore.feature.auth

import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.data.repo.user.UserRepository
import io.reactivex.Completable

class AuthViewModel(private val userRepository: UserRepository):NikeViewModel() {

    fun login(email:String, password:String):Completable{
        progressLiveData.value =true
        return userRepository.login(email,password).doFinally {
            progressLiveData.postValue(false)
        }
    }

    fun signUp(email:String, password:String):Completable{
        progressLiveData.value =true
        return userRepository.signUp(email,password).doFinally {
            progressLiveData.postValue(false)
        }
    }
}