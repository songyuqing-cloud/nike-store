package com.sajjad.nikestore.feature.product.comment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_ID
import com.sajjad.nikestore.common.NikeActivity
import com.sajjad.nikestore.data.Comment
import com.sajjad.nikestore.feature.product.CommentListAdapter
import kotlinx.android.synthetic.main.activity_comment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CommentListActivity : NikeActivity() {
    val commentListViewModel:CommentListViewModel by viewModel { parametersOf(intent.extras!!.getInt(
        EXTRA_KEY_ID)) }

    val commentListAdapter = CommentListAdapter(true)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_list)

        comment_rv.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)

        commentListViewModel.commentsLiveData.observe(this){
            commentListAdapter.comments = it as ArrayList<Comment>
        }

        commentListViewModel.progressLiveData.observe(this){
            setProgressIndicator(it)
        }

        comment_rv.adapter = commentListAdapter

        comment_list_toolbar.onBackClickListener = View.OnClickListener {
            finish()
        }
    }
}