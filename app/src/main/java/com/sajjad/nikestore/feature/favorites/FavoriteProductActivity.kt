package com.sajjad.nikestore.feature.favorites

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.common.NikeActivity
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.feature.product.ProductDetailActivity
import kotlinx.android.synthetic.main.activity_product_favorite.*
import kotlinx.android.synthetic.main.activity_product_list.*
import kotlinx.android.synthetic.main.view_default_empty_state.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject

class FavoriteProductActivity : NikeActivity(),
    FavoriteProductAdapter.FavoriteProductEventListener {

    val viewModel: FavoriteProductViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_favorite)

        viewModel.favProductLiveData.observe(this) {
            if (it.isNotEmpty()) {
                favorite_product_rv.layoutManager =
                    LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                favorite_product_rv.adapter =
                    FavoriteProductAdapter(it as MutableList<Product>, get(), this)
            } else {
                showEmptyState(R.layout.view_default_empty_state)
                empty_state_message_tv.text = getString(R.string.favorite_empty_state_message)
            }

        }

        help_btn.setOnClickListener {
            showSnackBar(getString(R.string.favorite_help_message))
        }

    }

    override fun onClick(product: Product) {
        startActivity(Intent(this, ProductDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA, product)
        })
    }

    override fun onLongClick(product: Product) {
        viewModel.remove(product)
        viewModel.favProductLiveData.observe(this) {

        }
    }
}