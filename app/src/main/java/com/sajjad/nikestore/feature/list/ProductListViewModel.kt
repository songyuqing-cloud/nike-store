package com.sajjad.nikestore.feature.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.NikeCompletableObserver
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.common.asyncNetworkRequest
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.data.repo.product.ProductRepository
import io.reactivex.schedulers.Schedulers

class ProductListViewModel(var sort: Int, val productRepository: ProductRepository) :
    NikeViewModel() {

    private val _productLiveData = MutableLiveData<List<Product>>()
    val productLiveData: LiveData<List<Product>>
        get() = _productLiveData


    private val _selectedSortTitleLiveData = MutableLiveData<Int>()
    val selectedSortTitleLiveData: LiveData<Int>
        get() = _selectedSortTitleLiveData

    val sortTitle = arrayOf(
        R.string.sort_latest,
        R.string.sort_popular,
        R.string.sort_price_low_to_high,
        R.string.sort_price_high_to_low
    )


    init {
        getProducts()
        _selectedSortTitleLiveData.value = sortTitle[sort]
    }

    fun getProducts() {
        progressLiveData.value = true
        productRepository.getProducts(sort)
            .asyncNetworkRequest()
            .doFinally {
                progressLiveData.value = false
            }
            .subscribe(object : NikeSingleObserver<List<Product>>(compositeDisposable) {
                override fun onSuccess(t: List<Product>) {
                    _productLiveData.value = t
                }

            })
    }

    fun changeSortByUser(sort: Int) {
        this.sort = sort
        this._selectedSortTitleLiveData.value = sortTitle[sort]
        getProducts()
    }

    fun addProductToFavorite(product: Product) {
        if (product.isFavorite) {
            productRepository.deleteFromFavorites(product)
                .subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        product.isFavorite = false
                    }
                })
        } else {
            productRepository.addToFavorite(product)
                .subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable) {
                    override fun onComplete() {
                        product.isFavorite = true
                    }
                })
        }
    }
}