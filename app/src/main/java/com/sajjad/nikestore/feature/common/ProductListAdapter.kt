package com.sajjad.nikestore.feature.common

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.formatPrice
import com.sajjad.nikestore.common.implementSpringAnimationTrait
import com.sajjad.nikestore.data.Product
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.view.NikeImageView
import java.lang.IllegalStateException

const val VIEW_TYPE_ROUND = 0
const val VIEW_TYPE_SMALL = 1
const val VIEW_TYPE_LARGE = 2

class ProductListAdapter(
    var viewType: Int = VIEW_TYPE_SMALL,
    private val imageLoadingService: ImageLoadingService
) : RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    var productEventListener: ProductEventListener? = null
    var products = ArrayList<Product>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutResId = when (viewType) {
            VIEW_TYPE_ROUND -> R.layout.item_product
            VIEW_TYPE_SMALL -> R.layout.item_product_small
            VIEW_TYPE_LARGE -> R.layout.item_product_large
            else -> throw IllegalStateException("View Type is not valid")
        }
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindProduct(products[position])

    override fun getItemCount(): Int = products.size

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val productIv: NikeImageView = itemView.findViewById(R.id.product_iv)
        val titleTv: TextView = itemView.findViewById(R.id.title_tv)
        val previousPriceTv: TextView = itemView.findViewById(R.id.product_previous_price_tv)
        val currentPriceTv: TextView = itemView.findViewById(R.id.product_current_price_tv)
        val favBtn: ImageView = itemView.findViewById(R.id.product_favorite_iv)

        fun bindProduct(product: Product) {
            imageLoadingService.load(productIv, product.image)
            titleTv.text = product.title
            previousPriceTv.text = formatPrice(product.previous_price)
            currentPriceTv.text = formatPrice(product.price)
            previousPriceTv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

            itemView.implementSpringAnimationTrait()
            itemView.setOnClickListener() {
                productEventListener?.onProductClick(product)
            }

            if (product.isFavorite)
                favBtn.setImageResource(R.drawable.ic_favorite_fill)
            else
                favBtn.setImageResource(R.drawable.ic_favorites)

            favBtn.setOnClickListener {
                productEventListener?.onFavoriteBtnClick(product)
                product.isFavorite = !product.isFavorite
                notifyItemChanged(adapterPosition)
            }

        }
    }

    interface ProductEventListener {
        fun onProductClick(product: Product)

        fun onFavoriteBtnClick(product: Product)
    }


}