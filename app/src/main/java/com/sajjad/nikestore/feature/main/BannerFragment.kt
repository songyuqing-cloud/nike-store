package com.sajjad.nikestore.feature.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.EXTRA_KEY_DATA
import com.sajjad.nikestore.data.Banner
import com.sajjad.nikestore.services.ImageLoadingService
import com.sajjad.nikestore.view.NikeImageView
import org.koin.android.ext.android.inject
import java.lang.IllegalStateException

class BannerFragment : Fragment() {
    val imageLoadingService: ImageLoadingService by inject()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val imageView = layoutInflater.inflate(R.layout.fragment_banner,container,false) as NikeImageView

        val banner = requireArguments().getParcelable<Banner>(EXTRA_KEY_DATA)?:throw IllegalStateException("banner cannot be null")

        imageLoadingService.load(imageView,banner.image)

        return imageView


    }

    companion object {
        fun newInstance(banner: Banner):BannerFragment {

            return BannerFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(EXTRA_KEY_DATA, banner)
                }
            }

            /*
            val bannerFragment = BannerFragment()
            val bundle = Bundle()

            bundle.putParcelable(EXTRA_KEY_DATA, banner)

            bannerFragment.arguments = bundle
            return bannerFragment

             */
        }
    }
}