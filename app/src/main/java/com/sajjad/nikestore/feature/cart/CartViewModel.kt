package com.sajjad.nikestore.feature.cart

import androidx.lifecycle.MutableLiveData
import com.sajjad.nikestore.R
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.common.asyncNetworkRequest
import com.sajjad.nikestore.data.EmptyState
import com.sajjad.nikestore.data.TokenContainer
import com.sajjad.nikestore.data.repo.cart.CartRepository
import com.sajjad.nikestore.data.CartItem
import com.sajjad.nikestore.data.CartItemCount
import com.sajjad.nikestore.data.CartResponse
import com.sajjad.nikestore.data.PurchaseDetail
import io.reactivex.Completable
import org.greenrobot.eventbus.EventBus

class CartViewModel(val cartRepository: CartRepository) : NikeViewModel() {
    val cartItemsLiveData = MutableLiveData<List<CartItem>>()
    var purchaseDetailLiveData = MutableLiveData<PurchaseDetail>()
    val emptyStateLiveData = MutableLiveData<EmptyState>()

    private fun getCartItems() {
        if (!TokenContainer.token.isNullOrEmpty()) {
            progressLiveData.value = true
            cartRepository.get()
                .asyncNetworkRequest()
                .doFinally {
                    progressLiveData.value = false
                }
                .subscribe(object : NikeSingleObserver<CartResponse>(compositeDisposable) {
                    override fun onSuccess(t: CartResponse) {
                        if (t.cart_items.isNotEmpty()) {
                            cartItemsLiveData.value = t.cart_items
                            purchaseDetailLiveData.value = PurchaseDetail(t.payable_price, t.shipping_cost, t.total_price)
                            emptyStateLiveData.value = EmptyState(false)
                        }else
                            emptyStateLiveData.value = EmptyState(true, R.string.cart_emptyState)
                    }

                })
        }else
            emptyStateLiveData.value = EmptyState(true,R.string.cart_emptyState_message_required,true)
    }

    fun removeFromCart(cartItem: CartItem): Completable {
        return cartRepository.remove(cartItem.cart_item_id)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
                cartItemsLiveData.value?.let {
                    if (it.isEmpty())
                        emptyStateLiveData.postValue(EmptyState(true,R.string.cart_emptyState))
                }

                val cartItemCount = EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count -= cartItem.count
                    EventBus.getDefault().postSticky(it)
                }



            }
            .ignoreElement()
    }

    fun increaseCartItemCount(cartItem: CartItem): Completable {

        return cartRepository.changeCount(cartItem.cart_item_id, ++cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
                val cartItemCount = EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count += 1
                    EventBus.getDefault().postSticky(it)
                }
            }
            .ignoreElement()
    }

    fun decreaseCartItemCount(cartItem: CartItem): Completable {
        return cartRepository.changeCount(cartItem.cart_item_id, --cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
                val cartItemCount = EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count -= 1
                    EventBus.getDefault().postSticky(it)
                }
            }
            .ignoreElement()
    }

    private fun calculateAndPublishPurchaseDetail() {
        cartItemsLiveData.value?.let { cartItems ->
            purchaseDetailLiveData.value?.let { purchaseDetail ->
                var totalPrice = 0
                var payablePrice = 0
                cartItems.forEach {
                    totalPrice += it.product.price * it.count
                    payablePrice += (it.product.price - it.product.discount) * it.count
                }
                purchaseDetail.total_price = totalPrice
                purchaseDetail.payable_price = payablePrice

                purchaseDetailLiveData.postValue(purchaseDetail)
            }
        }
    }

    fun refresh() {
        getCartItems()
    }

}