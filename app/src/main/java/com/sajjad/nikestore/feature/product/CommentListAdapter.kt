package com.sajjad.nikestore.feature.product

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sajjad.nikestore.R
import com.sajjad.nikestore.data.Comment

class CommentListAdapter(val showAll:Boolean) : RecyclerView.Adapter<CommentListAdapter.ViewHolder>() {

    var comments = ArrayList<Comment>()
    set(value)  {
        field = value
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(comments[position])


    override fun getItemCount(): Int = if (comments.size > 3 && !showAll) 3 else comments.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val titleTv: TextView = itemView.findViewById(R.id.comment_title_tv)
        val dateTv: TextView = itemView.findViewById(R.id.comment_date_tv)
        val authorTv: TextView = itemView.findViewById(R.id.comment_author_tv)
        val contentTv: TextView = itemView.findViewById(R.id.comment_content_tv)

        fun bind(comment: Comment) {
            titleTv.text = comment.title
            dateTv.text = comment.date
            authorTv.text = comment.author.email
            contentTv.text = comment.content
        }
    }


}