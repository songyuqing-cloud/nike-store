package com.sajjad.nikestore.feature.checkout

import androidx.lifecycle.MutableLiveData
import com.sajjad.nikestore.common.NikeSingleObserver
import com.sajjad.nikestore.common.NikeViewModel
import com.sajjad.nikestore.common.asyncNetworkRequest
import com.sajjad.nikestore.data.repo.order.OrderRepository
import com.sajjad.nikestore.data.Checkout

class CheckoutViewModel(orderId:Int, orderRepository: OrderRepository):NikeViewModel() {
    val checkoutLiveData = MutableLiveData<Checkout>()
    init {
        orderRepository.checkOut(orderId)
            .asyncNetworkRequest()
            .subscribe(object :NikeSingleObserver<Checkout>(compositeDisposable){
                override fun onSuccess(t: Checkout) {
                    checkoutLiveData.value = t
                }

            })
    }
}